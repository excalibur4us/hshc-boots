package com.hshc.repository.mysql.cms;

import com.hshc.cms.entity.TestDemo;
import org.springframework.stereotype.Repository;

/**
 * Created by admin on 2018/5/22.
 */
@Repository
public interface TestMySqlRepository {
    void insert(TestDemo testDemo);
}
