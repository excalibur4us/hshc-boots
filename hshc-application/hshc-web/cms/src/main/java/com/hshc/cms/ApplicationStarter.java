package com.hshc.cms;

import com.hshc.cms.config.ApplicationConfig;
import org.springframework.boot.SpringApplication;

/**
 * Created by admin on 2018/5/22.
 */
public class ApplicationStarter {
    public static void main(String[] args){
        SpringApplication.run(ApplicationConfig.class, args);
    }
}
