package com.hshc.cms.controller;

import com.hshc.cms.service.TestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by admin on 2018/5/22.
 */
@RestController
public class TestController {
    @Autowired
    private TestService testService;

    @RequestMapping("test")
    public String test(){
        return testService.test();
    }
}
