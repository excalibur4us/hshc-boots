package com.hshc.cms.config;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by admin on 2018/5/22.
 */
@SpringBootApplication
@ComponentScan("com.hshc")
public class ApplicationConfig implements CommandLineRunner {
    public void run(String... strings) throws Exception {
        System.out.print("hshc boot start up!!!");
    }
}
