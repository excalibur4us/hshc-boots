package com.hshc.cms.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by admin on 2018/5/22.
 */
@Component
@PropertySource(value = {"classpath:properties/base.properties"},encoding="utf-8")
public class BasePropertiesConfig {
    @Value("${home.tip}")
    private String homeTip;//主页欢迎文案
    @Value("${environment.current}")
    private String currentEnvironment;//当前环境

    public String getHomeTip() {
        return homeTip;
    }

    public String getCurrentEnvironment() {
        return currentEnvironment;
    }
}
