package com.hshc.cms.controller;

import com.hshc.cms.config.BasePropertiesConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by admin on 2018/5/22.
 */
@RestController
public class ApplicationHomeController {
    @Autowired
    private BasePropertiesConfig basePropertiesConfig;

    @RequestMapping("/")
    String home(){
        return basePropertiesConfig.getHomeTip()+basePropertiesConfig.getCurrentEnvironment();
    }
}
