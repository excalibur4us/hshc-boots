package com.hshc.cms.service;

import com.hshc.cms.entity.TestDemo;
import com.hshc.repository.mysql.cms.TestMySqlRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by admin on 2018/5/22.
 */
@Service
public class TestService {

    @Autowired
    private TestMySqlRepository testMySqlRepository;

    public String test(){
        TestDemo testDemo=new TestDemo();
        testDemo.setValue1("111111");
        testDemo.setValue2("22222");
        testMySqlRepository.insert(testDemo);
        return "yyyy";
    }
}
